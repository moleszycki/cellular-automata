package pl.oleszycki.mateusz.cellular.automata.ui;

import lombok.extern.slf4j.Slf4j;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

import javax.swing.*;
import java.awt.*;

@Slf4j
public class DrawPanel extends JPanel {

    private Space space;

    public DrawPanel() {
        setBackground(new Color(255, 255, 255));
    }

    @Override
    public void paint(Graphics g) {
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        super.paint(g);
        Graphics2D graphics2D = (Graphics2D) g;
        if (space != null) {
            space.getSpace().stream().forEach(cell -> {
                graphics2D.setColor(ColorMaker.getColor(cell.getGrainId()));
                graphics2D.drawRect(cell.getX(), cell.getY(), 1, 1);
            });
        } else {
            graphics2D.drawString("empty", 250, 250);
        }
    }

    public void setSpace(Space space) {
        this.space = space;
    }
}
