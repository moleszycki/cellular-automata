package pl.oleszycki.mateusz.cellular.automata.engine.impls;

import lombok.extern.slf4j.Slf4j;
import pl.oleszycki.mateusz.cellular.automata.engine.GrowAlgorithm;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

import java.util.List;

@Slf4j
public class BorderSpaceImpl implements GrowAlgorithm {

    @Override
    public void calcNewState(Cell cell, Space space) {
        List<Cell> neighborhood = getNeighborhood(cell, space);
        long count = neighborhood
                .stream()
                .filter(c -> c.getGrainId() == cell.getGrainId() || c.getGrainId() == Cell.BORDERS)
                .count();
        if (count < 8) {
            cell.setGrainId(Cell.BORDERS);
        }
    }

    private List<Cell> getNeighborhood(Cell cell, Space space) {
        return List.of(
                space.getCell(cell.getX(), cell.getY() + 1),
                space.getCell(cell.getX() + 1, cell.getY() + 1),
                space.getCell(cell.getX() - 1, cell.getY() + 1),
                space.getCell(cell.getX(), cell.getY() - 1),
                space.getCell(cell.getX() - 1, cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY()),
                space.getCell(cell.getX() - 1, cell.getY()));
    }

}
