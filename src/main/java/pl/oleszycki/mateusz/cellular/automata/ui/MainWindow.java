package pl.oleszycki.mateusz.cellular.automata.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import lombok.extern.slf4j.Slf4j;
import pl.oleszycki.mateusz.cellular.automata.cdi.Settings;
import pl.oleszycki.mateusz.cellular.automata.engine.TimeMachine;
import pl.oleszycki.mateusz.cellular.automata.engine.data.BoundaryCondition;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;
import pl.oleszycki.mateusz.cellular.automata.engine.impls.SupportedAlgorithm;
import pl.oleszycki.mateusz.cellular.automata.loaders.LoadFromCSV;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.text.NumberFormatter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.io.File;

@Slf4j
@Singleton
public class MainWindow extends JFrame {

    @Inject
    private TimeMachine timeMachine;

    @Inject
    private FormSpace formSpace;

    @Inject
    private Settings settings;

    private JPanel mainPanel;
    private JButton startButton;
    private JButton resetButton;
    private JTextField spaceSizeField;
    private JTextField initialField;
    private JPanel buttomGroup;
    private JComboBox algorithmComboBox;
    private JButton importButton;
    private JComboBox boundaryComboBox;
    private JCheckBox inclusionsCheckBox;
    private JFormattedTextField numberOfInclusionsField;
    private JFormattedTextField inclusionsMinSizeField;
    private JFormattedTextField inclusionsMaxSizeField;
    private JSlider gbcThresholdSlider;
    private JLabel gbcThresholdText;

    public MainWindow() {
        resetButton.addActionListener(e -> {
            updateSettings();
            timeMachine.reset();
        });
        startButton.addActionListener((actionEvent) -> {
            updateSettings();
            timeMachine.reset();
            formSpace.openWindow();
        });
        importButton.addActionListener(e -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("CSV File", "csv"));
            if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
                File file = fileChooser.getSelectedFile();
                LoadFromCSV loadFromCSV = new LoadFromCSV();
                Space space = loadFromCSV.load(file);
                timeMachine.loadSpace(space);
                formSpace.openWindow();
            }
        });
        inclusionsCheckBox.addActionListener(this::onChangeInclusionsCheckBox);
        gbcThresholdSlider.addChangeListener(this::onChangeGbcThresholdSlider);
        algorithmComboBox.addActionListener(this::onChangeAlgorithmComboBox);
        for (SupportedAlgorithm supportedAlgorithm : SupportedAlgorithm.values()) {
            algorithmComboBox.addItem(supportedAlgorithm.name());
        }
        for (BoundaryCondition boundaryCondition : BoundaryCondition.values()) {
            boundaryComboBox.addItem(boundaryCondition.name());
        }
        algorithmComboBox.addActionListener(this::updateSettingsOnChange);
    }

    private void onChangeAlgorithmComboBox(ActionEvent actionEvent) {
        if (SupportedAlgorithm.valueOf((String) algorithmComboBox.getSelectedItem()) == SupportedAlgorithm.GBC) {
            gbcThresholdSlider.setEnabled(true);
            boundaryComboBox.setSelectedItem(BoundaryCondition.NonPeriodic.name());
            boundaryComboBox.setEnabled(false);
        } else {
            gbcThresholdSlider.setEnabled(false);
            boundaryComboBox.setEnabled(true);
        }
    }

    private void onChangeGbcThresholdSlider(ChangeEvent e) {
        gbcThresholdText.setText(String.valueOf(gbcThresholdSlider.getValue()));
    }

    private void setNumberFormatterInTextFields() {

    }

    private NumberFormatter getPositiveFormatter() {
        NumberFormatter numberFormatter = new NumberFormatter();
        numberFormatter.setMinimum(0);
        numberFormatter.setMaximum(Integer.MAX_VALUE);
        return numberFormatter;
    }

    @PostConstruct
    public void postConstruct() {
        spaceSizeField.setText(String.valueOf(settings.getSize()));
        initialField.setText(String.valueOf(settings.getNumberOfSeed()));
        inclusionsCheckBox.setSelected(settings.isEnableInclusions());
        onChangeInclusionsCheckBox(null);
        numberOfInclusionsField.setText(String.valueOf(settings.getInclusionsNumber()));
        inclusionsMinSizeField.setText(String.valueOf(settings.getInclusionsMinSize()));
        inclusionsMaxSizeField.setText(String.valueOf(settings.getInclusionsMaxSize()));
    }

    private void onChangeInclusionsCheckBox(ActionEvent e) {
        numberOfInclusionsField.setEnabled(inclusionsCheckBox.isSelected());
        inclusionsMinSizeField.setEnabled(inclusionsCheckBox.isSelected());
        inclusionsMaxSizeField.setEnabled(inclusionsCheckBox.isSelected());
    }

    private void updateSettingsOnChange(AWTEvent actionEvent) {
        updateSettings();
    }

    public void openWindow() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());
        this.add(mainPanel);
        this.setSize(800, 600);
        this.setVisible(true);
    }

    private void updateSettings() {
        settings.setNumberOfSeed(Integer.parseInt(initialField.getText()));
        settings.setSize(Integer.parseInt(spaceSizeField.getText()));
        settings.setCurrentAlgorithm(SupportedAlgorithm.valueOf((String) algorithmComboBox.getSelectedItem()));
        settings.setCurrentBoundaryCondition(BoundaryCondition.valueOf((String) boundaryComboBox.getSelectedItem()));
        settings.setEnableInclusions(inclusionsCheckBox.isSelected());
        settings.setInclusionsNumber(Integer.parseInt(numberOfInclusionsField.getText()));
        settings.setInclusionsMinSize(Integer.parseInt(inclusionsMinSizeField.getText()));
        settings.setInclusionsMaxSize(Integer.parseInt(inclusionsMaxSizeField.getText()));
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayoutManager(8, 2, new Insets(5, 5, 5, 5), -1, -1));
        final Spacer spacer1 = new Spacer();
        mainPanel.add(spacer1, new GridConstraints(7, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, 1, null, null, null, 0, false));
        final Spacer spacer2 = new Spacer();
        mainPanel.add(spacer2, new GridConstraints(6, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Initial seed");
        mainPanel.add(label1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Space size");
        mainPanel.add(label2, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        spaceSizeField = new JTextField();
        mainPanel.add(spaceSizeField, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        initialField = new JTextField();
        mainPanel.add(initialField, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        buttomGroup = new JPanel();
        buttomGroup.setLayout(new GridLayoutManager(1, 4, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(buttomGroup, new GridConstraints(7, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        resetButton = new JButton();
        resetButton.setText("Reset");
        buttomGroup.add(resetButton, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final Spacer spacer3 = new Spacer();
        buttomGroup.add(spacer3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        startButton = new JButton();
        startButton.setText("Execute");
        buttomGroup.add(startButton, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        importButton = new JButton();
        importButton.setText("Import");
        buttomGroup.add(importButton, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label3 = new JLabel();
        label3.setText("Neighbourhood algorithm");
        mainPanel.add(label3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        algorithmComboBox = new JComboBox();
        mainPanel.add(algorithmComboBox, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label4 = new JLabel();
        label4.setText("Boundary condition");
        mainPanel.add(label4, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        boundaryComboBox = new JComboBox();
        mainPanel.add(boundaryComboBox, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label5 = new JLabel();
        label5.setText("Inclusions");
        mainPanel.add(label5, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JLabel label6 = new JLabel();
        label6.setText("GBC threshold");
        mainPanel.add(label6, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(panel1, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        gbcThresholdSlider = new JSlider();
        gbcThresholdSlider.setEnabled(true);
        gbcThresholdSlider.setMinimum(1);
        gbcThresholdSlider.setPaintLabels(true);
        gbcThresholdSlider.setPaintTicks(true);
        gbcThresholdSlider.setSnapToTicks(false);
        gbcThresholdSlider.setValueIsAdjusting(true);
        panel1.add(gbcThresholdSlider, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        gbcThresholdText = new JLabel();
        gbcThresholdText.setText("50");
        panel1.add(gbcThresholdText, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, new Dimension(20, -1), null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 2, new Insets(0, 0, 0, 0), -1, -1));
        mainPanel.add(panel2, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 8, new Insets(0, 0, 0, 0), -1, -1));
        panel2.add(panel3, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        inclusionsCheckBox = new JCheckBox();
        inclusionsCheckBox.setHorizontalAlignment(2);
        inclusionsCheckBox.setHorizontalTextPosition(4);
        inclusionsCheckBox.setText("Enable");
        panel3.add(inclusionsCheckBox, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label7 = new JLabel();
        label7.setText("Number");
        panel3.add(label7, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        numberOfInclusionsField = new JFormattedTextField();
        numberOfInclusionsField.setHorizontalAlignment(10);
        panel3.add(numberOfInclusionsField, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(100, -1), new Dimension(100, -1), 0, false));
        final JLabel label8 = new JLabel();
        label8.setText("Size");
        panel3.add(label8, new GridConstraints(0, 3, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(55, 16), null, 0, false));
        final JLabel label9 = new JLabel();
        label9.setText("min");
        panel3.add(label9, new GridConstraints(0, 4, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        inclusionsMinSizeField = new JFormattedTextField();
        inclusionsMinSizeField.putClientProperty("caretWidth", new Integer(-1));
        panel3.add(inclusionsMinSizeField, new GridConstraints(0, 5, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(50, -1), new Dimension(50, -1), 0, false));
        final JLabel label10 = new JLabel();
        label10.setText("max");
        panel3.add(label10, new GridConstraints(0, 6, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        inclusionsMaxSizeField = new JFormattedTextField();
        inclusionsMaxSizeField.putClientProperty("caretWidth", new Integer(-1));
        panel3.add(inclusionsMaxSizeField, new GridConstraints(0, 7, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(50, -1), new Dimension(50, -1), 0, false));
        final Spacer spacer4 = new Spacer();
        panel2.add(spacer4, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return mainPanel;
    }

}
