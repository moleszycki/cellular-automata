package pl.oleszycki.mateusz.cellular.automata.ui.tools;

import java.awt.event.MouseEvent;

public interface MouseClicked {
    void mouseClicked(MouseEvent e);
}
