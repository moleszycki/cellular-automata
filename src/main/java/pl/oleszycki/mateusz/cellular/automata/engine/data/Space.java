package pl.oleszycki.mateusz.cellular.automata.engine.data;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import pl.oleszycki.mateusz.cellular.automata.cdi.Settings;

import javax.enterprise.inject.spi.CDI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Slf4j
public class Space {
    private int x, y;
    private List<Cell> space;
    private Map<String, Cell> fastSearch;
    private Settings settings = CDI.current().select(Settings.class).get();
    private int currentPhase = 0;

    public Cell getCell(int x, int y) {
        switch (settings.getCurrentBoundaryCondition()) {
            case NonPeriodic:
                return fastSearch.getOrDefault(Cell.searchHash(x, y), new Cell(0, 0, Cell.BORDERS));
            case Periodic:
                return getNonPeriodic(x,y);
        }
        throw new IllegalStateException("Not supported Boundary Condition");
    }

    private Cell getNonPeriodic(int x, int y) {
        return fastSearch.getOrDefault(Cell.searchHash(x % this.x, y % this.y), new Cell(0, 0));
    }

    public void removeGrain(int x, int y) {
        Cell cell = fastSearch.getOrDefault(Cell.searchHash(x, y), new Cell(0, 0));
        int grainId = cell.getGrainId();
        if(grainId != Cell.EMPTY && grainId != Cell.INCLUSIONS) {
            space.parallelStream()
                    .filter(c -> c.getGrainId() == grainId)
                    .forEach(c -> c.setGrainId(Cell.EMPTY));
        }
    }

    public void addNewSeeds(int numberOfNewSeeds) {
        int beginIndex = findMaxGrainId();
        for (int i = 0; i < numberOfNewSeeds; i++) {
            boolean added = false;
            while(!added) {
                int x = RandomUtils.nextInt(0, settings.getSize());
                int y = RandomUtils.nextInt(0, settings.getSize());
                if(this.getCell(x, y).getGrainId() == Cell.EMPTY) {
                    this.getCell(x, y).setGrainId(beginIndex + i + 1);
                    this.getCell(x, y).setPhase(currentPhase);
                    added = true;
                }
            }
        }
    }

    private int findMaxGrainId() {
        return space
                .parallelStream()
                .filter(c-> c.getGrainId() != Cell.INCLUSIONS)
                .mapToInt(Cell::getGrainId)
                .max().orElse(0);
    }

    public Space cloneSpace() {
        Space space = new Space();
        space.x = x;
        space.y = y;
        List<Cell> cellList = new ArrayList<>();
        Map<String, Cell> searchMap = new HashMap<>();
        this.getSpace()
                .forEach(cell -> {
                    try {
                        Cell cloned = cell.clone();
                        cellList.add((cloned));
                        searchMap.put(cloned.searchHash(), cloned);
                    } catch (CloneNotSupportedException e) {
                        e.printStackTrace();
                    }
                });
        space.setSpace(cellList);
        space.setFastSearch(searchMap);
        return space;
    }
}
