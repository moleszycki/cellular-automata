package pl.oleszycki.mateusz.cellular.automata.engine.impls;

import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import pl.oleszycki.mateusz.cellular.automata.cdi.Settings;
import pl.oleszycki.mateusz.cellular.automata.engine.GrowAlgorithm;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

import javax.enterprise.inject.spi.CDI;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class GBCImpl implements GrowAlgorithm {

    private Settings settings = CDI.current().select(Settings.class).get();

    @Override
    public void calcNewState(Cell cell, Space space) {
        List<Integer> grainIdNeighborhood = new ArrayList<>(8);
        getNeighborhoodRule1(cell, space)
                .stream()
                .filter(c -> Commons.isHotCell(c, space.getCurrentPhase()))
                .forEach(c -> grainIdNeighborhood.add(c.getGrainId()));
        Map<Integer, Long> neighborhoodType = grainIdNeighborhood
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        Pair<Integer, Integer> neighborhoodMostPopular = findPopularTypeNeighborhood(neighborhoodType);
        if (neighborhoodMostPopular.getRight() >= 5) {
            cell.setGrainId(neighborhoodMostPopular.getLeft());
            cell.setPhase(space.getCurrentPhase());
            return;
        }

        grainIdNeighborhood.clear();
        getNeighborhoodRule2(cell, space)
                .stream()
                .filter(c -> Commons.isHotCell(c, space.getCurrentPhase()))
                .forEach(c -> grainIdNeighborhood.add(c.getGrainId()));
        neighborhoodType = grainIdNeighborhood
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        neighborhoodMostPopular = findPopularTypeNeighborhood(neighborhoodType);
        if (neighborhoodMostPopular.getRight() >= 3) {
            cell.setGrainId(neighborhoodMostPopular.getLeft());
            cell.setPhase(space.getCurrentPhase());
            return;
        }

        grainIdNeighborhood.clear();
        getNeighborhoodRule3(cell, space)
                .stream()
                .filter(c -> Commons.isHotCell(c, space.getCurrentPhase()))
                .forEach(c -> grainIdNeighborhood.add(c.getGrainId()));
        neighborhoodType = grainIdNeighborhood
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        neighborhoodMostPopular = findPopularTypeNeighborhood(neighborhoodType);
        if (neighborhoodMostPopular.getRight() >= 3) {
            cell.setGrainId(neighborhoodMostPopular.getLeft());
            cell.setPhase(space.getCurrentPhase());
            return;
        }

        grainIdNeighborhood.clear();
        getNeighborhoodRule4(cell, space)
                .stream()
                .filter(c -> Commons.isHotCell(c, space.getCurrentPhase()))
                .forEach(c -> grainIdNeighborhood.add(c.getGrainId()));
        neighborhoodType = grainIdNeighborhood
                .stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        neighborhoodMostPopular = findPopularTypeNeighborhood(neighborhoodType);
        if (RandomUtils.nextInt(0, 100) <= settings.getGBCThreshold()) {
            cell.setGrainId(neighborhoodMostPopular.getLeft());
            cell.setPhase(space.getCurrentPhase());
            return;
        }
    }

    private Pair<Integer, Integer> findPopularTypeNeighborhood(Map<Integer, Long> neighborhoodMap) {
        long popularInt = 0;
        int mostPopularGrainId = Cell.EMPTY;
        for (Integer currGrainId : neighborhoodMap.keySet()) {
            if (neighborhoodMap.get(currGrainId) > popularInt) {
                popularInt = neighborhoodMap.get(currGrainId);
                mostPopularGrainId = currGrainId;
            }
        }
        return ImmutablePair.of(mostPopularGrainId, (int) popularInt);
    }

    private List<Cell> getNeighborhoodRule1(Cell cell, Space space) {
        return List.of(
                space.getCell(cell.getX(), cell.getY() + 1),
                space.getCell(cell.getX() + 1, cell.getY() + 1),
                space.getCell(cell.getX() - 1, cell.getY() + 1),
                space.getCell(cell.getX(), cell.getY() - 1),
                space.getCell(cell.getX() - 1, cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY()),
                space.getCell(cell.getX() - 1, cell.getY()));
    }

    private List<Cell> getNeighborhoodRule2(Cell cell, Space space) {
        return List.of(
                space.getCell(cell.getX(), cell.getY() + 1),
                space.getCell(cell.getX(), cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY()),
                space.getCell(cell.getX() - 1, cell.getY()));
    }

    private List<Cell> getNeighborhoodRule3(Cell cell, Space space) {
        return List.of(
                space.getCell(cell.getX() + 1, cell.getY() + 1),
                space.getCell(cell.getX() - 1, cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY() - 1),
                space.getCell(cell.getX() - 1, cell.getY() + 1));
    }

    private List<Cell> getNeighborhoodRule4(Cell cell, Space space) {
        return List.of(
                space.getCell(cell.getX(), cell.getY() + 1),
                space.getCell(cell.getX() + 1, cell.getY() + 1),
                space.getCell(cell.getX() - 1, cell.getY() + 1),
                space.getCell(cell.getX(), cell.getY() - 1),
                space.getCell(cell.getX() - 1, cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY()),
                space.getCell(cell.getX() - 1, cell.getY()));
    }
}
