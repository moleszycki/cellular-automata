package pl.oleszycki.mateusz.cellular.automata.engine.impls;

import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;

import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

class Commons {

    static Integer findPopularTypeNeighborhood(Map<Integer, Long> neighborhoodMap) {
        AtomicReference<Integer> grainId = new AtomicReference<>(Cell.EMPTY);
        AtomicReference<Integer> popular = new AtomicReference<>(0);
        neighborhoodMap.forEach((k, v) -> {
            if (popular.get() < v) {
                grainId.set(k);
                popular.set(v.intValue());
            }
        });
        return grainId.get();
    }

    // filter cell non empty non inclusions in current phase.
    static boolean isHotCell(Cell c, int currentPhase) {
        return (c.getGrainId() != Cell.EMPTY && c.getGrainId() != Cell.INCLUSIONS && c.getGrainId() != Cell.BORDERS && c.getPhase() == currentPhase);
    }

    static boolean isHotCell(Cell c) {
        return (c.getGrainId() != Cell.EMPTY && c.getGrainId() != Cell.INCLUSIONS && c.getGrainId() != Cell.BORDERS);
    }

}
