package pl.oleszycki.mateusz.cellular.automata.cdi;

import lombok.extern.slf4j.Slf4j;
import pl.oleszycki.mateusz.cellular.automata.engine.GrowAlgorithm;
import pl.oleszycki.mateusz.cellular.automata.engine.impls.GBCImpl;
import pl.oleszycki.mateusz.cellular.automata.engine.impls.MooreImpl;
import pl.oleszycki.mateusz.cellular.automata.engine.impls.VonNeumannaGrowImpl;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.enterprise.inject.Typed;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class AlgorithmProducer {

    private VonNeumannaGrowImpl vonNeumannaGrow = new VonNeumannaGrowImpl();
    private MooreImpl moore = new MooreImpl();
    private GBCImpl gbc = new GBCImpl();

    @Inject
    private Settings settings;

    @Typed(GrowAlgorithm.class)
    @Produces
    public GrowAlgorithm takeGrowAlgorithm() {
        switch (settings.getCurrentAlgorithm()) {
            case Moore:
                return moore;
            case VonNeumanna:
                return vonNeumannaGrow;
            case GBC:
                return gbc;
        }
        return vonNeumannaGrow;
    }

}
