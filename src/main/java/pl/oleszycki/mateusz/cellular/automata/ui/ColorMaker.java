package pl.oleszycki.mateusz.cellular.automata.ui;

import org.apache.commons.lang3.RandomUtils;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;

import java.awt.*;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ColorMaker {

    private static Map<Integer, Color> colors = new ConcurrentHashMap<>();

    public static Color getColor(int numberColor) {
        if (numberColor == Cell.EMPTY) {
            return new Color(255, 255, 255);
        }
        if(numberColor == Cell.INCLUSIONS) {
            return new Color(75, 75, 75);
        }
        if(numberColor == Cell.BORDERS) {
            return new Color(0, 0, 0);
        }
        if (!colors.containsKey(numberColor)) {
            colors.put(numberColor, new Color(RandomUtils.nextInt(0, 255), RandomUtils.nextInt(0, 255), RandomUtils.nextInt(0, 255)));
        }
        return colors.get(numberColor);
    }
}
