package pl.oleszycki.mateusz.cellular.automata.ui;

import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.math.NumberUtils;
import pl.oleszycki.mateusz.cellular.automata.cdi.Settings;
import pl.oleszycki.mateusz.cellular.automata.engine.TimeMachine;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Statistic;
import pl.oleszycki.mateusz.cellular.automata.exports.SaveSpace;
import pl.oleszycki.mateusz.cellular.automata.exports.SaveToCSV;
import pl.oleszycki.mateusz.cellular.automata.exports.SaveToPNG;
import pl.oleszycki.mateusz.cellular.automata.ui.tools.MouseOnClick;

import javax.enterprise.event.ObservesAsync;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

@Slf4j
@Singleton
public class FormSpace extends JFrame {

    @Inject
    private Settings settings;

    @Inject
    private TimeMachine timeMachine;

    private JPanel main;
    private JPanel render;
    private JButton runButton;
    private JButton stepButton;
    private JButton saveButton;
    private JButton cleanButton;
    private JCheckBox removeGrainCheckBox;
    private JButton addSeedButton;
    private JButton statisticButton;
    private JButton borderButton;
    private DrawPanel drawPanel = new DrawPanel();

    public FormSpace() {
        log.info("init form space");
        render.add(drawPanel);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                log.info("Render closed");
                onClose();
            }
        });
        runButton.addActionListener(this::onClickStartStop);
        cleanButton.addActionListener(this::onClickClean);
        stepButton.addActionListener(this::onClickStep);
        saveButton.addActionListener(this::onSave);
        drawPanel.addMouseListener(new MouseOnClick(this::onClickRender));
        removeGrainCheckBox.addActionListener(this::onChangeRemoveGrain);
        addSeedButton.addActionListener(this::onClickAddSeed);
        borderButton.addActionListener(this::onClickBorder);
        statisticButton.addActionListener(this::onClickStat);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }

    public void openWindow() {
        this.setLayout(new BorderLayout());
        this.add(main);
        this.setSize(settings.getSize() + 28, settings.getSize() + 108);
        this.setVisible(true);
    }

    @SneakyThrows
    private void onClickStartStop(ActionEvent event) {
        if (timeMachine.isRunning()) {
            timeMachine.stop();
            runButton.setText("Run");
        } else {
            new Thread(() -> {
                timeMachine.start();
            }).start();
            runButton.setText("Stop");
        }
        Thread.sleep(100);
        stepButton.setEnabled(!timeMachine.isRunning());
        saveButton.setEnabled(!timeMachine.isRunning());
        cleanButton.setEnabled(!timeMachine.isRunning());
        removeGrainCheckBox.setEnabled(!timeMachine.isRunning());
        addSeedButton.setEnabled(!timeMachine.isRunning());
        statisticButton.setEnabled(!timeMachine.isRunning());
        borderButton.setEnabled(!timeMachine.isRunning());
    }

    private void onClose() {
        timeMachine.stop();
        runButton.setText("Run");
    }

    private void onSave(ActionEvent event) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setAcceptAllFileFilterUsed(false);
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("PNG Images", "png"));
        fileChooser.addChoosableFileFilter(new FileNameExtensionFilter("CSV File", "csv"));
        if (fileChooser.showSaveDialog(this) == JFileChooser.APPROVE_OPTION) {
            SaveSpace saveSpace;
            String extension;
            FileFilter currentFilter = fileChooser.getFileFilter();
            if (currentFilter.getDescription().equals("CSV File")) {
                saveSpace = new SaveToCSV();
                extension = ".csv";
            } else {
                saveSpace = new SaveToPNG();
                extension = ".png";
            }
            File file = fileChooser.getSelectedFile();
            if (!file.getName().endsWith(extension)) {
                file = new File(file.getAbsolutePath() + extension);
            }
            saveSpace.saveSpace(file, timeMachine.getCurrentSpaces());
        }
    }

    private void onClickClean(ActionEvent event) {
        timeMachine.clean();
    }

    private void onClickStep(ActionEvent event) {
        timeMachine.step();
    }

    private void onChangeRemoveGrain(ActionEvent event) {
        runButton.setEnabled(!removeGrainCheckBox.isSelected());
        cleanButton.setEnabled(!removeGrainCheckBox.isSelected());
        stepButton.setEnabled(!removeGrainCheckBox.isSelected());
        saveButton.setEnabled(!removeGrainCheckBox.isSelected());
        statisticButton.setEnabled(!removeGrainCheckBox.isSelected());
        borderButton.setEnabled(!removeGrainCheckBox.isSelected());
        addSeedButton.setEnabled(!removeGrainCheckBox.isSelected());
        if (!removeGrainCheckBox.isSelected()) {
            timeMachine.nextPhase();
        }
    }

    private void onClickRender(MouseEvent mouseEvent) {
        if (removeGrainCheckBox.isSelected()) {
            timeMachine.removeGrain(mouseEvent.getX(), mouseEvent.getY());
        }
    }

    private void onClickAddSeed(ActionEvent actionEvent) {
        String answer = JOptionPane.showInputDialog("How many add seed to space");
        if (answer != null) {
            if (NumberUtils.isDigits(answer)) {
                Integer numberOfSeed = Integer.parseInt(answer);
                timeMachine.addSeed(numberOfSeed);
            }
        }
    }

    private void onClickBorder(ActionEvent actionEvent) {
        timeMachine.makeBorders();
    }

    private void onClickStat(ActionEvent actionEvent) {
        Statistic statistic = timeMachine.stat();
        JOptionPane.showMessageDialog(this, "Average seed: " + statistic.getAvgSeed() +
                "\n border length: " + statistic.getLongBorder());
    }

    public void drawSpace(@ObservesAsync Space space) throws Exception {
        SwingUtilities.invokeAndWait(() -> {
            drawPanel.setSpace(space);
            drawPanel.repaint();
        });
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        main = new JPanel();
        main.setLayout(new GridLayoutManager(3, 6, new Insets(5, 5, 5, 5), -1, -1));
        render = new JPanel();
        render.setLayout(new BorderLayout(0, 0));
        render.setBackground(new Color(-1));
        main.add(render, new GridConstraints(0, 0, 1, 6, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        render.setBorder(BorderFactory.createTitledBorder(BorderFactory.createLineBorder(new Color(-16777216)), null));
        final Spacer spacer1 = new Spacer();
        main.add(spacer1, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, 1, null, null, null, 0, false));
        runButton = new JButton();
        runButton.setText("Run");
        main.add(runButton, new GridConstraints(2, 5, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        stepButton = new JButton();
        stepButton.setText("Step");
        main.add(stepButton, new GridConstraints(2, 4, 1, 1, GridConstraints.ANCHOR_EAST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        saveButton = new JButton();
        saveButton.setText("Save");
        main.add(saveButton, new GridConstraints(1, 5, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        removeGrainCheckBox = new JCheckBox();
        removeGrainCheckBox.setText("Remove grain");
        main.add(removeGrainCheckBox, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        addSeedButton = new JButton();
        addSeedButton.setText("Add seed");
        main.add(addSeedButton, new GridConstraints(1, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        statisticButton = new JButton();
        statisticButton.setText("Statistic");
        main.add(statisticButton, new GridConstraints(1, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        cleanButton = new JButton();
        cleanButton.setText("Clean");
        main.add(cleanButton, new GridConstraints(2, 3, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(77, 24), null, 0, false));
        borderButton = new JButton();
        borderButton.setText("Border");
        main.add(borderButton, new GridConstraints(1, 4, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return main;
    }

}
