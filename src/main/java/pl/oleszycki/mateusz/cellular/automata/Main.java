package pl.oleszycki.mateusz.cellular.automata;

import lombok.extern.slf4j.Slf4j;
import org.jboss.weld.environment.se.StartMain;

import javax.swing.*;

@Slf4j
public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("hello");
        log.info("hello");
        UIManager.LookAndFeelInfo[] looks = UIManager.getInstalledLookAndFeels();
        for (UIManager.LookAndFeelInfo look : looks) {
            if (look.getClassName().equals("com.sun.java.swing.plaf.windows.WindowsLookAndFeel")) {
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            }
        }
        StartMain.main(args);
    }

}
