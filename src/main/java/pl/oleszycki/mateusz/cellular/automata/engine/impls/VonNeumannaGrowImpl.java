package pl.oleszycki.mateusz.cellular.automata.engine.impls;

import lombok.extern.slf4j.Slf4j;
import pl.oleszycki.mateusz.cellular.automata.engine.GrowAlgorithm;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

import javax.enterprise.inject.Typed;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;
import java.util.stream.Collectors;

@Slf4j
@Typed(VonNeumannaGrowImpl.class)
public class VonNeumannaGrowImpl implements GrowAlgorithm {

    @Override
    public void calcNewState(final Cell cell, Space space) {
        List<Integer> grainIdNeighborhood = new ArrayList<>(4);
        getNeighborhood(cell, space)
                .stream()
                .filter(c -> Commons.isHotCell(c, space.getCurrentPhase()))
                .forEach(c -> grainIdNeighborhood.add(c.getGrainId()));
        if (!grainIdNeighborhood.isEmpty()) {
            Map<Integer, Long> neighborhoodType = grainIdNeighborhood
                    .stream()
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            cell.setGrainId(Commons.findPopularTypeNeighborhood(neighborhoodType));
            cell.setPhase(space.getCurrentPhase());
        }
    }

    private List<Cell> getNeighborhood(Cell cell, Space space) {
        return List.of(
                space.getCell(cell.getX(), cell.getY() + 1),
                space.getCell(cell.getX(), cell.getY() - 1),
                space.getCell(cell.getX() + 1, cell.getY()),
                space.getCell(cell.getX() - 1, cell.getY()));
    }
}
