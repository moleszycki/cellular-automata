package pl.oleszycki.mateusz.cellular.automata.exports;

import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

import java.io.File;

public interface SaveSpace {

    void saveSpace(File file, Space space);

}
