package pl.oleszycki.mateusz.cellular.automata.engine.impls;

public enum SupportedAlgorithm {
    VonNeumanna,
    Moore,
    GBC;
}
