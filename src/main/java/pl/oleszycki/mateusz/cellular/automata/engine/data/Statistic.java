package pl.oleszycki.mateusz.cellular.automata.engine.data;

import lombok.Data;

@Data
public class Statistic {
    private int longBorder;
    private double avgSeed;
}
