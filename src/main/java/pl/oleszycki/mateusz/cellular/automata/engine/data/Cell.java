package pl.oleszycki.mateusz.cellular.automata.engine.data;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
public class Cell implements Serializable, Cloneable {
    public static final int EMPTY = 0;
    public static final int INCLUSIONS = Integer.MAX_VALUE;
    public static final int BORDERS = Integer.MAX_VALUE - 1;
    private int grainId = EMPTY;
    private int x;
    private int y;
    private int phase;

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Cell(int x, int y, int grainId) {
        this.grainId = grainId;
        this.x = x;
        this.y = y;
    }

    public Cell(int x, int y, int grainId, int phase) {
        this.grainId = grainId;
        this.x = x;
        this.y = y;
        this.phase = phase;
    }

    public static String searchHash(int x, int y) {
        return x + ":" + y;
    }

    @Override
    public Cell clone() throws CloneNotSupportedException {
        return (Cell) super.clone();
    }

    public String searchHash() {
        return searchHash(x, y);
    }
}
