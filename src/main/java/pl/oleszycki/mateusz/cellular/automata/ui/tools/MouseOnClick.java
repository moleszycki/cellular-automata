package pl.oleszycki.mateusz.cellular.automata.ui.tools;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class MouseOnClick implements MouseListener {

    private MouseClicked mouseClicked;

    public MouseOnClick(MouseClicked mouseClicked) {
        this.mouseClicked = mouseClicked;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        mouseClicked.mouseClicked(e);
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}
