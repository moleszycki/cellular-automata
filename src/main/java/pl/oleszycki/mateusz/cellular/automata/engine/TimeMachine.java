package pl.oleszycki.mateusz.cellular.automata.engine;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import pl.oleszycki.mateusz.cellular.automata.cdi.InitSpaceProducer;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Statistic;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.util.stream.Collectors;

@Slf4j
@ApplicationScoped
public class TimeMachine {

    @Inject
    private Engine engine;

    private Space zeroSpace;
    @Getter
    private Space currentSpaces;
    private boolean running = false;

    @Inject
    private InitSpaceProducer initSpaceProducer;

    @Inject
    private Event<Space> spaceEvent;

    public void start() {
        running = true;
        loop();
    }

    public void stop() {
        running = false;
    }

    public void loop() {
        if (currentSpaces == null) {
            currentSpaces = initSpaceProducer.initSpace();
            zeroSpace = currentSpaces;
        }
        while (running) {
            step();
        }
    }

    public void step() {
        currentSpaces = engine.calcNewSpace(currentSpaces);
        spaceEvent.fireAsync(currentSpaces);
    }

    public void clean() {
        currentSpaces = zeroSpace;
        spaceEvent.fireAsync(currentSpaces);
    }

    public void reset() {
        currentSpaces = initSpaceProducer.initSpace();
        zeroSpace = currentSpaces;
        spaceEvent.fireAsync(currentSpaces);
    }

    public void loadSpace(Space space) {
        currentSpaces = space;
        zeroSpace = currentSpaces;
        spaceEvent.fireAsync(currentSpaces);
    }

    public void removeGrain(int x, int y) {
        currentSpaces.removeGrain(x, y);
        spaceEvent.fireAsync(currentSpaces);
    }

    public void nextPhase() {
        currentSpaces.setCurrentPhase(currentSpaces.getCurrentPhase() + 1);
    }

    public void addSeed(int number) {
        currentSpaces.addNewSeeds(number);
        spaceEvent.fireAsync(currentSpaces);
    }

    public void makeBorders() {
        currentSpaces = engine.makeBorders(currentSpaces);
        spaceEvent.fireAsync(currentSpaces);
    }

    public boolean isRunning() {
        return running;
    }

    public Statistic stat() {
        Statistic statistic = new Statistic();
        long cells = currentSpaces
                .getSpace()
                .stream()
                .filter(c -> c.getGrainId() != Cell.EMPTY && c.getGrainId() != Cell.INCLUSIONS && c.getGrainId() != Cell.BORDERS)
                .count();
        double numberOfGrain = currentSpaces
                .getSpace()
                .stream()
                .filter(c -> c.getGrainId() != Cell.EMPTY && c.getGrainId() != Cell.INCLUSIONS && c.getGrainId() != Cell.BORDERS)
                .map(Cell::getGrainId)
                .collect(Collectors.toSet())
                .size();
        statistic.setAvgSeed(cells / numberOfGrain);
        statistic.setLongBorder((int) (currentSpaces
                .getSpace()
                .stream()
                .filter(c -> c.getGrainId() == Cell.BORDERS)
                .count() / 2));
        return statistic;
    }

}
