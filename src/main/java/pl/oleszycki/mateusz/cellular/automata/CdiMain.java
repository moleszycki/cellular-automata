package pl.oleszycki.mateusz.cellular.automata;

import lombok.extern.slf4j.Slf4j;
import org.jboss.weld.environment.se.events.ContainerInitialized;
import pl.oleszycki.mateusz.cellular.automata.ui.MainWindow;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.event.Observes;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class CdiMain {

    @Inject
    private MainWindow mainWindow;

    public void onStartup(@Observes ContainerInitialized startEvent) {
        log.info("CDI works");
        mainWindow.openWindow();
    }

}
