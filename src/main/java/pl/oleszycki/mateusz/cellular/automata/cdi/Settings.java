package pl.oleszycki.mateusz.cellular.automata.cdi;

import lombok.Data;
import pl.oleszycki.mateusz.cellular.automata.engine.data.BoundaryCondition;
import pl.oleszycki.mateusz.cellular.automata.engine.impls.SupportedAlgorithm;

import javax.enterprise.context.ApplicationScoped;

@Data
@ApplicationScoped
public class Settings {
    private int numberOfSeed = 5;
    private int size = 500;
    private SupportedAlgorithm currentAlgorithm = SupportedAlgorithm.VonNeumanna;
    private BoundaryCondition currentBoundaryCondition = BoundaryCondition.NonPeriodic;
    private boolean enableInclusions = false;
    private int inclusionsNumber = 5;
    private int inclusionsMinSize = 1;
    private int inclusionsMaxSize = 10;
    private int GBCThreshold = 50;
}
