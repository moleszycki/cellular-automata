package pl.oleszycki.mateusz.cellular.automata.exports;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.StringJoiner;

import static java.lang.String.valueOf;

@Slf4j
public class SaveToCSV implements SaveSpace {

    @SneakyThrows
    @Override
    public void saveSpace(File file, Space space) {
        StringBuilder stringBuilder = new StringBuilder();
        space.getSpace().forEach(cell -> {
            StringJoiner joiner = new StringJoiner(",");
            joiner.add(valueOf(cell.getX())).add(valueOf(cell.getY())).add(valueOf(cell.getGrainId())).add(valueOf(cell.getPhase()));
            stringBuilder.append(joiner.toString()).append(System.lineSeparator());
        });
        FileUtils.writeStringToFile(file, stringBuilder.toString(), StandardCharsets.UTF_8, false);
    }
}
