package pl.oleszycki.mateusz.cellular.automata.loaders;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class LoadFromCSV {

    @SneakyThrows
    public Space load(File file) {
        Space space = new Space();
        List<Cell> cells = new ArrayList<>();
        List<String> lineCsv = FileUtils.readLines(file, StandardCharsets.UTF_8);
        lineCsv.forEach(line -> {
            String[] values = line.split(",");
            if (values.length == 3) {
                cells.add(new Cell(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2])));
            } else {
                if (values.length == 4) {
                    cells.add(new Cell(Integer.parseInt(values[0]), Integer.parseInt(values[1]), Integer.parseInt(values[2]), Integer.parseInt(values[3])));
                } else {
                    log.warn("unsupported cell from csv. skip line");
                }
            }
        });
        space.setSpace(cells);
        space.setCurrentPhase(findMaxGrainId(cells));
        Map<String, Cell> searchMap = new HashMap<>();
        space.getSpace()
                .forEach(c -> {
                    searchMap.put(c.searchHash(), c);
                });
        space.setFastSearch(searchMap);
        cells.stream().mapToInt(Cell::getX).max().ifPresent(space::setX);
        cells.stream().mapToInt(Cell::getY).max().ifPresent(space::setY);
        return space;
    }

    private int findMaxGrainId(List<Cell> cells) {
        return cells
                .parallelStream()
                .filter(c -> c.getGrainId() != Cell.INCLUSIONS)
                .mapToInt(Cell::getGrainId)
                .max().orElse(0);
    }
}
