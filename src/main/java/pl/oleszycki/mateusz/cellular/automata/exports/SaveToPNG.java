package pl.oleszycki.mateusz.cellular.automata.exports;

import lombok.SneakyThrows;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;
import pl.oleszycki.mateusz.cellular.automata.ui.ColorMaker;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class SaveToPNG implements SaveSpace {

    @SneakyThrows
    @Override
    public void saveSpace(File file, Space space) {
        BufferedImage bufferedImage = new BufferedImage(space.getX(), space.getY(), BufferedImage.TYPE_INT_RGB);
        Graphics2D graphics2D = bufferedImage.createGraphics();
        space.getSpace().stream().forEach(cell -> {
            graphics2D.setColor(ColorMaker.getColor(cell.getGrainId()));
            graphics2D.drawRect(cell.getX(), cell.getY(), 1, 1);
        });
        ImageIO.write(bufferedImage, "PNG", file);
    }

}
