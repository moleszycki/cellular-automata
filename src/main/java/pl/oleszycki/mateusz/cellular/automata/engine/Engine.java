package pl.oleszycki.mateusz.cellular.automata.engine;

import lombok.extern.slf4j.Slf4j;
import pl.oleszycki.mateusz.cellular.automata.cdi.AlgorithmProducer;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;
import pl.oleszycki.mateusz.cellular.automata.engine.impls.BorderSpaceImpl;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@Slf4j
@ApplicationScoped
public class Engine {

    @Inject
    private AlgorithmProducer algorithmProducer;

    private Space spaceA;
    private Space spaceB;

    public Space calcNewSpace(Space oldSpace) {
        GrowAlgorithm growAlgorithm = algorithmProducer.takeGrowAlgorithm();
        if ((spaceA == null && spaceB == null) || (spaceA != oldSpace && spaceB != oldSpace)) {
            spaceA = oldSpace.cloneSpace();
            spaceB = oldSpace.cloneSpace();
        }
        Space workingSpace;
        if (spaceA == oldSpace) {
            workingSpace = spaceB;
        } else {
            workingSpace = spaceA;
        }
        copy(oldSpace, workingSpace);
        workingSpace.getSpace()
                .parallelStream()
                .filter(cell -> cell.getGrainId() == (Cell.EMPTY))
                .forEach(cell -> growAlgorithm.calcNewState(cell, oldSpace));
        return workingSpace;
    }

    public Space makeBorders(Space oldSpace) {
        GrowAlgorithm growAlgorithm = new BorderSpaceImpl();
        if ((spaceA == null && spaceB == null) || (spaceA != oldSpace && spaceB != oldSpace)) {
            spaceA = oldSpace.cloneSpace();
            spaceB = oldSpace.cloneSpace();
        }
        Space workingSpace;
        if (spaceA == oldSpace) {
            workingSpace = spaceB;
        } else {
            workingSpace = spaceA;
        }
        copy(oldSpace, workingSpace);
        workingSpace.getSpace()
                .parallelStream()
                .forEach(cell -> growAlgorithm.calcNewState(cell, oldSpace));
        return workingSpace;
    }

    private void copy(Space from, Space to) {
        to.setCurrentPhase(from.getCurrentPhase());
        from.getSpace().parallelStream().forEach(cell -> to.getFastSearch().get(Cell.searchHash(cell.getX(), cell.getY())).setGrainId(cell.getGrainId()));
    }
}
