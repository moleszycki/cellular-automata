package pl.oleszycki.mateusz.cellular.automata.cdi;

import org.apache.commons.lang3.RandomUtils;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@ApplicationScoped
public class InitSpaceProducer {

    @Inject
    private Settings settings;

    @Named("init_space")
    @Produces
    public Space initSpace() {
        Space initSpace = initEmptySpace();
        addRandomSeeds(initSpace);
        addInclusions(initSpace);
        return initSpace;
    }

    private Space initEmptySpace() {
        Space initSpace = new Space();
        initSpace.setX(settings.getSize());
        initSpace.setY(settings.getSize());
        initSpace.setSpace(new ArrayList<>());
        for (int x = 0; x < settings.getSize(); x++) {
            for (int y = 0; y < settings.getSize(); y++) {
                initSpace.getSpace().add(new Cell(x, y));
            }
        }
        Map<String, Cell> searchMap = new HashMap<>();
        initSpace.getSpace()
                .stream()
                .forEach(c -> {
                    searchMap.put(c.searchHash(), c);
                });
        initSpace.setFastSearch(searchMap);
        return initSpace;
    }

    private void addRandomSeeds(Space space) {
        space.addNewSeeds(settings.getNumberOfSeed());
    }

    private void addInclusions(Space space) {
        if(settings.isEnableInclusions()) {
            for(int i=0; i < settings.getInclusionsNumber(); i++) {
                int x = RandomUtils.nextInt(0, settings.getSize());
                int y = RandomUtils.nextInt(0, settings.getSize());
                int size = RandomUtils.nextInt(settings.getInclusionsMinSize(), settings.getInclusionsMaxSize());
                for(int rx = -size; rx < size; rx++) {
                    for(int ry = -size; ry < size; ry++) {
                        if((rx * rx) + (ry * ry) <= size * size) {
                            space.getCell(x + rx, y + ry).setGrainId(Cell.INCLUSIONS);
                        }
                    }
                }
            }
        }
    }
}
