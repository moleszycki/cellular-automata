package pl.oleszycki.mateusz.cellular.automata.engine;

import pl.oleszycki.mateusz.cellular.automata.engine.data.Cell;
import pl.oleszycki.mateusz.cellular.automata.engine.data.Space;

public interface GrowAlgorithm {
    void calcNewState(Cell cell, Space space);
}
