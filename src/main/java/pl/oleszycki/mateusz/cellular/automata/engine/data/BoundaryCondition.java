package pl.oleszycki.mateusz.cellular.automata.engine.data;

public enum BoundaryCondition {
    NonPeriodic,
    Periodic;
}
